import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:investfolio/navigation/investfolio_router.dart';
import 'package:nuvigator/nuvigator.dart';

const _accessTokenKey = "investfolio.accessToken";
const _refreshTokenKey = "investfolio.refreshToken";

class AuthBloc extends ChangeNotifier {
  AuthBloc() : _storage = FlutterSecureStorage();

  final FlutterSecureStorage _storage;

  Future<String> get accessToken async {
    var accessToken = await _storage.read(key: _accessTokenKey);
    if (accessToken == null) return '';
    return accessToken;
  }

  Future<String> get refreshToken async {
    var refreshToken = await _storage.read(key: _refreshTokenKey);
    if (refreshToken == null) return '';
    return refreshToken;
  }

  Future<void> revokeTokens(BuildContext context) async {
    await _storage.deleteAll();
    notifyListeners();
    Router.of<InvestfolioRouter>(context).pushReplacementToLoginScreen();
  }

  Future<void> setTokens(String accessToken, String refreshToken) async {
    await _setAccessToken(accessToken);
    await _setRefreshToken(refreshToken);
    notifyListeners();
  }

  Future<void> _setAccessToken(String token) async {
    await _storage.write(key: _accessTokenKey, value: token);
  }

  Future<void> _setRefreshToken(String token) async {
    await _storage.write(key: _refreshTokenKey, value: token);
  }
}
