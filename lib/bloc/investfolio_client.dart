import 'package:artemis/artemis.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:investfolio/bloc/auth_bloc.dart';
import 'package:investfolio/blocs/graphql_client.dart';
import 'package:http/http.dart' as http;
import 'package:investfolio/graphql/create_portfolio.dart' as create;
import 'package:investfolio/graphql/update_portfolio.dart' as update;
import 'package:investfolio/graphql/list_portfolios.dart';
import 'package:investfolio/graphql/login.dart';
import 'package:investfolio/graphql/refresh_tokens.dart';
import 'package:investfolio/graphql/remove_portfolio.dart';
import 'package:investfolio/graphql/sign_up.dart';
import 'package:investfolio/models/request_response.dart';

import '../utilities.dart';

class InvestfolioClient extends IGraphQLClient {
  InvestfolioClient(this._authBloc)
      : _httpClient = AuthenticatedClient(_authBloc);

  final AuthBloc _authBloc;
  final http.Client _httpClient;

  final TCallbackWith2Params<ArtemisClient, http.Client, String>
      _getInvestfolioClient = memoizedArtemisClient();

  @override
  Future<AuthBloc> get authBloc async => _authBloc;

  @override
  Future<http.Client> get httpClient async => _httpClient;

  @override
  Future<ArtemisClient> get investfolioClient async =>
      _getInvestfolioClient(_httpClient, 'http://localhost:4000/api');

  @override
  Future<bool> login(String email, String password) async {
    final mutation = LoginMutation(
      variables: LoginArguments(
        email: email,
        password: password,
      ),
    );
    final response = await (await investfolioClient).execute(mutation);

    final credentials = response.data?.login;
    if (credentials != null) {
      await (await authBloc)
          .setTokens(credentials.accessToken, credentials.refreshToken);
      return true;
    } else
      return false;
  }

  @override
  Future<RequestResponse> signUp(
      String name, String email, String password) async {
    final mutation = SignUpMutation(
      variables: SignUpArguments(
        input: CreateUserInput(
          name: name,
          email: email,
          emailConfirmation: email,
          password: password,
          passwordConfirmation: password,
        ),
      ),
    );

    final response = await (await investfolioClient).execute(mutation);
    return RequestResponse(
      success: response.data?.createUser?.success,
      errors: response.data?.createUser?.errors,
    );
  }

  @override
  Future<List<Portfolio>> listPortfolios(BuildContext context) async {
    final query = ListPortfoliosQuery();
    final response = await _requestWithRefresh(context, query);
    return response.data?.viewer?.portfolios;
  }

  @override
  Future<RemovePortfolioResult> removePortfolio(
      BuildContext context, String portfolioId) async {
    final query = RemovePortfolioMutation(
      variables: RemovePortfolioArguments(portfolioId: portfolioId),
    );
    final response = await _requestWithRefresh(context, query);
    return response.data?.removePortfolio;
  }

  @override
  Future<RequestResponse> createPortfolio(
      BuildContext context, Portfolio portfolio) async {
    final query = create.CreatePortfolioMutation(
      variables: create.CreatePortfolioArguments(
        input: create.CreatePortfolioInput(
          name: portfolio.name,
          description: portfolio.description ?? '',
          status: EnumToString.fromString(
            create.PortfoliosStatus.values,
            EnumToString.parse(portfolio.status),
          ),
        ),
      ),
    );
    final response = await _requestWithRefresh(context, query);
    return RequestResponse(
      success: response.data?.createPortfolio?.success,
      errors: response.data?.createPortfolio?.errors,
    );
  }

  @override
  Future<RequestResponse> updatePortfolio(
      BuildContext context, Portfolio portfolio) async {
    final query = update.UpdatePortfolioMutation(
      variables: update.UpdatePortfolioArguments(
        input: update.UpdatePortfolioInput(
          portfolioId: portfolio.id,
          name: portfolio.name,
          description: portfolio.description ?? '',
          status: EnumToString.fromString(
            update.PortfoliosStatus.values,
            EnumToString.parse(portfolio.status),
          ),
        ),
      ),
    );
    final response = await _requestWithRefresh(context, query);
    return RequestResponse(
      success: response.data?.updatePortfolio?.success,
      errors: response.data?.updatePortfolio?.errors,
    );
  }

  Future<GraphQLResponse> _requestWithRefresh(
      BuildContext context, GraphQLQuery query) async {
    final response = await (await investfolioClient).execute(query);
    if (response.hasErrors &&
        response.errors.any((e) => e.message == 'unauthorized')) {
      if (await _refreshToken(context)) {
        final response = await (await investfolioClient).execute(query);
        return response;
      }
    }

    return response;
  }

  Future<bool> _refreshToken(BuildContext context) async {
    final mutation = RefreshTokensMutation(
      variables: RefreshTokensArguments(
        accessToken: await (await authBloc).accessToken,
        refreshToken: await (await authBloc).refreshToken,
      ),
    );

    final response = await (await investfolioClient).execute(mutation);
    final credentials = response.data?.refresh;
    if (credentials != null) {
      await (await authBloc).setTokens(
        credentials.accessToken,
        credentials.refreshToken,
      );
      return true;
    } else {
      await (await authBloc).revokeTokens(context);
      return false;
    }
  }
}

class AuthenticatedClient extends http.BaseClient {
  AuthenticatedClient(this._authBloc);

  final AuthBloc _authBloc;
  final http.Client _httpClient = http.Client();

  Future<AuthBloc> get authBloc async => _authBloc;

  Future<http.StreamedResponse> send(http.BaseRequest request) async {
    final accessToken = await (await authBloc).accessToken;
    if (accessToken != "")
      request.headers['Authorization'] = 'Bearer $accessToken';
    return _httpClient.send(request);
  }
}
