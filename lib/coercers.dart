import 'package:decimal/decimal.dart';
import 'package:intl/intl.dart';

final dateFormatter = DateFormat('yyyy-MM-dd');
final timeFormatter = DateFormat('HH:mm:ss');

// Date <-> DateTime
DateTime fromGraphQLDateToDartDateTime(String date) => DateTime.parse(date);
String fromDartDateTimeToGraphQLDate(DateTime date) =>
    dateFormatter.format(date);

// Time <-> DateTime
DateTime fromGraphQLTimeToDartDateTime(String time) =>
    DateTime.parse('1970-01-01T${time}Z');
String fromDartDateTimeToGraphQLTime(DateTime date) =>
    timeFormatter.format(date);

// Decimal <-> Decimal
Decimal fromGraphQLDecimalToDartDecimal(String decimal) =>
    Decimal.parse(decimal);

String fromDartDecimalToGraphQLDecimal(Decimal decimal) => '$decimal';
