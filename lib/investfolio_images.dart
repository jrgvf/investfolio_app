import 'package:flutter_svg/flutter_svg.dart';

abstract class InvestfolioImages {
  const InvestfolioImages._();

  static SvgPicture fullLogo = SvgPicture.asset(
    'assets/images/full-logo.svg',
    semanticsLabel: 'Investfolio, sua carteira de investimentos',
  );
}
