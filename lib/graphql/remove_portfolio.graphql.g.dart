// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'remove_portfolio.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RemovePortfolioResult _$RemovePortfolioResultFromJson(
    Map<String, dynamic> json) {
  return RemovePortfolioResult()..success = json['success'] as bool;
}

Map<String, dynamic> _$RemovePortfolioResultToJson(
        RemovePortfolioResult instance) =>
    <String, dynamic>{
      'success': instance.success,
    };

RootMutationType _$RootMutationTypeFromJson(Map<String, dynamic> json) {
  return RootMutationType()
    ..removePortfolio = json['removePortfolio'] == null
        ? null
        : RemovePortfolioResult.fromJson(
            json['removePortfolio'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RootMutationTypeToJson(RootMutationType instance) =>
    <String, dynamic>{
      'removePortfolio': instance.removePortfolio?.toJson(),
    };

RemovePortfolioArguments _$RemovePortfolioArgumentsFromJson(
    Map<String, dynamic> json) {
  return RemovePortfolioArguments(
    portfolioId: json['portfolioId'] as String,
  );
}

Map<String, dynamic> _$RemovePortfolioArgumentsToJson(
        RemovePortfolioArguments instance) =>
    <String, dynamic>{
      'portfolioId': instance.portfolioId,
    };
