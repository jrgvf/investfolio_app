// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_portfolio.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InputError _$InputErrorFromJson(Map<String, dynamic> json) {
  return InputError()
    ..key = json['key'] as String
    ..message = json['message'] as String;
}

Map<String, dynamic> _$InputErrorToJson(InputError instance) =>
    <String, dynamic>{
      'key': instance.key,
      'message': instance.message,
    };

UpdatePortfolioResult _$UpdatePortfolioResultFromJson(
    Map<String, dynamic> json) {
  return UpdatePortfolioResult()
    ..success = json['success'] as bool
    ..errors = (json['errors'] as List)
        ?.map((e) =>
            e == null ? null : InputError.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$UpdatePortfolioResultToJson(
        UpdatePortfolioResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'errors': instance.errors?.map((e) => e?.toJson())?.toList(),
    };

RootMutationType _$RootMutationTypeFromJson(Map<String, dynamic> json) {
  return RootMutationType()
    ..updatePortfolio = json['updatePortfolio'] == null
        ? null
        : UpdatePortfolioResult.fromJson(
            json['updatePortfolio'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RootMutationTypeToJson(RootMutationType instance) =>
    <String, dynamic>{
      'updatePortfolio': instance.updatePortfolio?.toJson(),
    };

UpdatePortfolioInput _$UpdatePortfolioInputFromJson(Map<String, dynamic> json) {
  return UpdatePortfolioInput(
    description: json['description'] as String,
    name: json['name'] as String,
    portfolioId: json['portfolioId'] as String,
    status: _$enumDecodeNullable(_$PortfoliosStatusEnumMap, json['status'],
        unknownValue: PortfoliosStatus.ARTEMIS_UNKNOWN),
  );
}

Map<String, dynamic> _$UpdatePortfolioInputToJson(
        UpdatePortfolioInput instance) =>
    <String, dynamic>{
      'description': instance.description,
      'name': instance.name,
      'portfolioId': instance.portfolioId,
      'status': _$PortfoliosStatusEnumMap[instance.status],
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$PortfoliosStatusEnumMap = {
  PortfoliosStatus.ACTIVE: 'ACTIVE',
  PortfoliosStatus.ARCHIVED: 'ARCHIVED',
  PortfoliosStatus.INACTIVE: 'INACTIVE',
  PortfoliosStatus.ARTEMIS_UNKNOWN: 'ARTEMIS_UNKNOWN',
};

UpdatePortfolioArguments _$UpdatePortfolioArgumentsFromJson(
    Map<String, dynamic> json) {
  return UpdatePortfolioArguments(
    input: json['input'] == null
        ? null
        : UpdatePortfolioInput.fromJson(json['input'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$UpdatePortfolioArgumentsToJson(
        UpdatePortfolioArguments instance) =>
    <String, dynamic>{
      'input': instance.input?.toJson(),
    };
