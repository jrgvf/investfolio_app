// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:meta/meta.dart';
import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:investfolio/coercers.dart';
import 'package:decimal/decimal.dart';
part 'create_portfolio.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class InputError with EquatableMixin {
  InputError();

  factory InputError.fromJson(Map<String, dynamic> json) =>
      _$InputErrorFromJson(json);

  String key;

  String message;

  @override
  List<Object> get props => [key, message];
  Map<String, dynamic> toJson() => _$InputErrorToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreatePortfolioResult with EquatableMixin {
  CreatePortfolioResult();

  factory CreatePortfolioResult.fromJson(Map<String, dynamic> json) =>
      _$CreatePortfolioResultFromJson(json);

  bool success;

  List<InputError> errors;

  @override
  List<Object> get props => [success, errors];
  Map<String, dynamic> toJson() => _$CreatePortfolioResultToJson(this);
}

@JsonSerializable(explicitToJson: true)
class RootMutationType with EquatableMixin {
  RootMutationType();

  factory RootMutationType.fromJson(Map<String, dynamic> json) =>
      _$RootMutationTypeFromJson(json);

  CreatePortfolioResult createPortfolio;

  @override
  List<Object> get props => [createPortfolio];
  Map<String, dynamic> toJson() => _$RootMutationTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreatePortfolioInput with EquatableMixin {
  CreatePortfolioInput({this.description, @required this.name, this.status});

  factory CreatePortfolioInput.fromJson(Map<String, dynamic> json) =>
      _$CreatePortfolioInputFromJson(json);

  String description;

  String name;

  @JsonKey(unknownEnumValue: PortfoliosStatus.ARTEMIS_UNKNOWN)
  PortfoliosStatus status;

  @override
  List<Object> get props => [description, name, status];
  Map<String, dynamic> toJson() => _$CreatePortfolioInputToJson(this);
}

enum PortfoliosStatus {
  ACTIVE,
  ARCHIVED,
  INACTIVE,
  ARTEMIS_UNKNOWN,
}

@JsonSerializable(explicitToJson: true)
class CreatePortfolioArguments extends JsonSerializable with EquatableMixin {
  CreatePortfolioArguments({@required this.input});

  factory CreatePortfolioArguments.fromJson(Map<String, dynamic> json) =>
      _$CreatePortfolioArgumentsFromJson(json);

  final CreatePortfolioInput input;

  @override
  List<Object> get props => [input];
  Map<String, dynamic> toJson() => _$CreatePortfolioArgumentsToJson(this);
}

class CreatePortfolioMutation
    extends GraphQLQuery<RootMutationType, CreatePortfolioArguments> {
  CreatePortfolioMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'createPortfolio'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'input')),
              type: NamedTypeNode(
                  name: NameNode(value: 'CreatePortfolioInput'),
                  isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'createPortfolio'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: VariableNode(name: NameNode(value: 'input')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'success'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'errors'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'key'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'message'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'createPortfolio';

  @override
  final CreatePortfolioArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  RootMutationType parse(Map<String, dynamic> json) =>
      RootMutationType.fromJson(json);
}
