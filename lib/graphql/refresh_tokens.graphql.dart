// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:meta/meta.dart';
import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:investfolio/coercers.dart';
import 'package:decimal/decimal.dart';
part 'refresh_tokens.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class Session with EquatableMixin {
  Session();

  factory Session.fromJson(Map<String, dynamic> json) =>
      _$SessionFromJson(json);

  String accessToken;

  String refreshToken;

  @override
  List<Object> get props => [accessToken, refreshToken];
  Map<String, dynamic> toJson() => _$SessionToJson(this);
}

@JsonSerializable(explicitToJson: true)
class RootMutationType with EquatableMixin {
  RootMutationType();

  factory RootMutationType.fromJson(Map<String, dynamic> json) =>
      _$RootMutationTypeFromJson(json);

  Session refresh;

  @override
  List<Object> get props => [refresh];
  Map<String, dynamic> toJson() => _$RootMutationTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class RefreshTokensArguments extends JsonSerializable with EquatableMixin {
  RefreshTokensArguments(
      {@required this.accessToken, @required this.refreshToken});

  factory RefreshTokensArguments.fromJson(Map<String, dynamic> json) =>
      _$RefreshTokensArgumentsFromJson(json);

  final String accessToken;

  final String refreshToken;

  @override
  List<Object> get props => [accessToken, refreshToken];
  Map<String, dynamic> toJson() => _$RefreshTokensArgumentsToJson(this);
}

class RefreshTokensMutation
    extends GraphQLQuery<RootMutationType, RefreshTokensArguments> {
  RefreshTokensMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'refresh_tokens'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'accessToken')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: []),
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'refreshToken')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'refresh'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'accessToken'),
                    value: VariableNode(name: NameNode(value: 'accessToken'))),
                ArgumentNode(
                    name: NameNode(value: 'refreshToken'),
                    value: VariableNode(name: NameNode(value: 'refreshToken')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'accessToken'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'refreshToken'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'refresh_tokens';

  @override
  final RefreshTokensArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  RootMutationType parse(Map<String, dynamic> json) =>
      RootMutationType.fromJson(json);
}
