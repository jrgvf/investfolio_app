// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:meta/meta.dart';
import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:investfolio/coercers.dart';
import 'package:decimal/decimal.dart';
part 'update_portfolio.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class InputError with EquatableMixin {
  InputError();

  factory InputError.fromJson(Map<String, dynamic> json) =>
      _$InputErrorFromJson(json);

  String key;

  String message;

  @override
  List<Object> get props => [key, message];
  Map<String, dynamic> toJson() => _$InputErrorToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdatePortfolioResult with EquatableMixin {
  UpdatePortfolioResult();

  factory UpdatePortfolioResult.fromJson(Map<String, dynamic> json) =>
      _$UpdatePortfolioResultFromJson(json);

  bool success;

  List<InputError> errors;

  @override
  List<Object> get props => [success, errors];
  Map<String, dynamic> toJson() => _$UpdatePortfolioResultToJson(this);
}

@JsonSerializable(explicitToJson: true)
class RootMutationType with EquatableMixin {
  RootMutationType();

  factory RootMutationType.fromJson(Map<String, dynamic> json) =>
      _$RootMutationTypeFromJson(json);

  UpdatePortfolioResult updatePortfolio;

  @override
  List<Object> get props => [updatePortfolio];
  Map<String, dynamic> toJson() => _$RootMutationTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UpdatePortfolioInput with EquatableMixin {
  UpdatePortfolioInput(
      {this.description,
      @required this.name,
      @required this.portfolioId,
      this.status});

  factory UpdatePortfolioInput.fromJson(Map<String, dynamic> json) =>
      _$UpdatePortfolioInputFromJson(json);

  String description;

  String name;

  String portfolioId;

  @JsonKey(unknownEnumValue: PortfoliosStatus.ARTEMIS_UNKNOWN)
  PortfoliosStatus status;

  @override
  List<Object> get props => [description, name, portfolioId, status];
  Map<String, dynamic> toJson() => _$UpdatePortfolioInputToJson(this);
}

enum PortfoliosStatus {
  ACTIVE,
  ARCHIVED,
  INACTIVE,
  ARTEMIS_UNKNOWN,
}

@JsonSerializable(explicitToJson: true)
class UpdatePortfolioArguments extends JsonSerializable with EquatableMixin {
  UpdatePortfolioArguments({@required this.input});

  factory UpdatePortfolioArguments.fromJson(Map<String, dynamic> json) =>
      _$UpdatePortfolioArgumentsFromJson(json);

  final UpdatePortfolioInput input;

  @override
  List<Object> get props => [input];
  Map<String, dynamic> toJson() => _$UpdatePortfolioArgumentsToJson(this);
}

class UpdatePortfolioMutation
    extends GraphQLQuery<RootMutationType, UpdatePortfolioArguments> {
  UpdatePortfolioMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'updatePortfolio'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'input')),
              type: NamedTypeNode(
                  name: NameNode(value: 'UpdatePortfolioInput'),
                  isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'updatePortfolio'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: VariableNode(name: NameNode(value: 'input')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'success'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'errors'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'key'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'message'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'updatePortfolio';

  @override
  final UpdatePortfolioArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  RootMutationType parse(Map<String, dynamic> json) =>
      RootMutationType.fromJson(json);
}
