// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_portfolios.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Portfolio _$PortfolioFromJson(Map<String, dynamic> json) {
  return Portfolio()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..description = json['description'] as String
    ..status = _$enumDecodeNullable(_$PortfoliosStatusEnumMap, json['status'],
        unknownValue: PortfoliosStatus.ARTEMIS_UNKNOWN);
}

Map<String, dynamic> _$PortfolioToJson(Portfolio instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'status': _$PortfoliosStatusEnumMap[instance.status],
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$PortfoliosStatusEnumMap = {
  PortfoliosStatus.ACTIVE: 'ACTIVE',
  PortfoliosStatus.ARCHIVED: 'ARCHIVED',
  PortfoliosStatus.INACTIVE: 'INACTIVE',
  PortfoliosStatus.ARTEMIS_UNKNOWN: 'ARTEMIS_UNKNOWN',
};

User _$UserFromJson(Map<String, dynamic> json) {
  return User()
    ..portfolios = (json['portfolios'] as List)
        ?.map((e) =>
            e == null ? null : Portfolio.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'portfolios': instance.portfolios?.map((e) => e?.toJson())?.toList(),
    };

RootQueryType _$RootQueryTypeFromJson(Map<String, dynamic> json) {
  return RootQueryType()
    ..viewer = json['viewer'] == null
        ? null
        : User.fromJson(json['viewer'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RootQueryTypeToJson(RootQueryType instance) =>
    <String, dynamic>{
      'viewer': instance.viewer?.toJson(),
    };
