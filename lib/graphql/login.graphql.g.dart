// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Session _$SessionFromJson(Map<String, dynamic> json) {
  return Session()
    ..accessToken = json['accessToken'] as String
    ..refreshToken = json['refreshToken'] as String;
}

Map<String, dynamic> _$SessionToJson(Session instance) => <String, dynamic>{
      'accessToken': instance.accessToken,
      'refreshToken': instance.refreshToken,
    };

RootMutationType _$RootMutationTypeFromJson(Map<String, dynamic> json) {
  return RootMutationType()
    ..login = json['login'] == null
        ? null
        : Session.fromJson(json['login'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RootMutationTypeToJson(RootMutationType instance) =>
    <String, dynamic>{
      'login': instance.login?.toJson(),
    };

LoginArguments _$LoginArgumentsFromJson(Map<String, dynamic> json) {
  return LoginArguments(
    email: json['email'] as String,
    password: json['password'] as String,
  );
}

Map<String, dynamic> _$LoginArgumentsToJson(LoginArguments instance) =>
    <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
    };
