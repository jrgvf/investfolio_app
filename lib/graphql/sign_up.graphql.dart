// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:meta/meta.dart';
import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:investfolio/coercers.dart';
import 'package:decimal/decimal.dart';
part 'sign_up.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class InputError with EquatableMixin {
  InputError();

  factory InputError.fromJson(Map<String, dynamic> json) =>
      _$InputErrorFromJson(json);

  String key;

  String message;

  @override
  List<Object> get props => [key, message];
  Map<String, dynamic> toJson() => _$InputErrorToJson(this);
}

@JsonSerializable(explicitToJson: true)
class User with EquatableMixin {
  User();

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  String name;

  String email;

  @override
  List<Object> get props => [name, email];
  Map<String, dynamic> toJson() => _$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateUserResult with EquatableMixin {
  CreateUserResult();

  factory CreateUserResult.fromJson(Map<String, dynamic> json) =>
      _$CreateUserResultFromJson(json);

  List<InputError> errors;

  bool success;

  User user;

  @override
  List<Object> get props => [errors, success, user];
  Map<String, dynamic> toJson() => _$CreateUserResultToJson(this);
}

@JsonSerializable(explicitToJson: true)
class RootMutationType with EquatableMixin {
  RootMutationType();

  factory RootMutationType.fromJson(Map<String, dynamic> json) =>
      _$RootMutationTypeFromJson(json);

  CreateUserResult createUser;

  @override
  List<Object> get props => [createUser];
  Map<String, dynamic> toJson() => _$RootMutationTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CreateUserInput with EquatableMixin {
  CreateUserInput(
      {@required this.email,
      @required this.emailConfirmation,
      @required this.name,
      @required this.password,
      @required this.passwordConfirmation});

  factory CreateUserInput.fromJson(Map<String, dynamic> json) =>
      _$CreateUserInputFromJson(json);

  String email;

  String emailConfirmation;

  String name;

  String password;

  String passwordConfirmation;

  @override
  List<Object> get props =>
      [email, emailConfirmation, name, password, passwordConfirmation];
  Map<String, dynamic> toJson() => _$CreateUserInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class SignUpArguments extends JsonSerializable with EquatableMixin {
  SignUpArguments({@required this.input});

  factory SignUpArguments.fromJson(Map<String, dynamic> json) =>
      _$SignUpArgumentsFromJson(json);

  final CreateUserInput input;

  @override
  List<Object> get props => [input];
  Map<String, dynamic> toJson() => _$SignUpArgumentsToJson(this);
}

class SignUpMutation extends GraphQLQuery<RootMutationType, SignUpArguments> {
  SignUpMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'signUp'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'input')),
              type: NamedTypeNode(
                  name: NameNode(value: 'CreateUserInput'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'createUser'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: VariableNode(name: NameNode(value: 'input')))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'errors'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'key'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'message'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ])),
                FieldNode(
                    name: NameNode(value: 'success'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null),
                FieldNode(
                    name: NameNode(value: 'user'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'name'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'email'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'signUp';

  @override
  final SignUpArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  RootMutationType parse(Map<String, dynamic> json) =>
      RootMutationType.fromJson(json);
}
