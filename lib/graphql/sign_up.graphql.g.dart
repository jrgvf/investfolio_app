// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sign_up.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InputError _$InputErrorFromJson(Map<String, dynamic> json) {
  return InputError()
    ..key = json['key'] as String
    ..message = json['message'] as String;
}

Map<String, dynamic> _$InputErrorToJson(InputError instance) =>
    <String, dynamic>{
      'key': instance.key,
      'message': instance.message,
    };

User _$UserFromJson(Map<String, dynamic> json) {
  return User()
    ..name = json['name'] as String
    ..email = json['email'] as String;
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'name': instance.name,
      'email': instance.email,
    };

CreateUserResult _$CreateUserResultFromJson(Map<String, dynamic> json) {
  return CreateUserResult()
    ..errors = (json['errors'] as List)
        ?.map((e) =>
            e == null ? null : InputError.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..success = json['success'] as bool
    ..user = json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>);
}

Map<String, dynamic> _$CreateUserResultToJson(CreateUserResult instance) =>
    <String, dynamic>{
      'errors': instance.errors?.map((e) => e?.toJson())?.toList(),
      'success': instance.success,
      'user': instance.user?.toJson(),
    };

RootMutationType _$RootMutationTypeFromJson(Map<String, dynamic> json) {
  return RootMutationType()
    ..createUser = json['createUser'] == null
        ? null
        : CreateUserResult.fromJson(json['createUser'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RootMutationTypeToJson(RootMutationType instance) =>
    <String, dynamic>{
      'createUser': instance.createUser?.toJson(),
    };

CreateUserInput _$CreateUserInputFromJson(Map<String, dynamic> json) {
  return CreateUserInput(
    email: json['email'] as String,
    emailConfirmation: json['emailConfirmation'] as String,
    name: json['name'] as String,
    password: json['password'] as String,
    passwordConfirmation: json['passwordConfirmation'] as String,
  );
}

Map<String, dynamic> _$CreateUserInputToJson(CreateUserInput instance) =>
    <String, dynamic>{
      'email': instance.email,
      'emailConfirmation': instance.emailConfirmation,
      'name': instance.name,
      'password': instance.password,
      'passwordConfirmation': instance.passwordConfirmation,
    };

SignUpArguments _$SignUpArgumentsFromJson(Map<String, dynamic> json) {
  return SignUpArguments(
    input: json['input'] == null
        ? null
        : CreateUserInput.fromJson(json['input'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$SignUpArgumentsToJson(SignUpArguments instance) =>
    <String, dynamic>{
      'input': instance.input?.toJson(),
    };
