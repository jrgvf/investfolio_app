// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:meta/meta.dart';
import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:investfolio/coercers.dart';
import 'package:decimal/decimal.dart';
part 'list_portfolios.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class Portfolio with EquatableMixin {
  Portfolio();

  factory Portfolio.fromJson(Map<String, dynamic> json) =>
      _$PortfolioFromJson(json);

  String id;

  String name;

  String description;

  @JsonKey(unknownEnumValue: PortfoliosStatus.ARTEMIS_UNKNOWN)
  PortfoliosStatus status;

  @override
  List<Object> get props => [id, name, description, status];
  Map<String, dynamic> toJson() => _$PortfolioToJson(this);
}

@JsonSerializable(explicitToJson: true)
class User with EquatableMixin {
  User();

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  List<Portfolio> portfolios;

  @override
  List<Object> get props => [portfolios];
  Map<String, dynamic> toJson() => _$UserToJson(this);
}

@JsonSerializable(explicitToJson: true)
class RootQueryType with EquatableMixin {
  RootQueryType();

  factory RootQueryType.fromJson(Map<String, dynamic> json) =>
      _$RootQueryTypeFromJson(json);

  User viewer;

  @override
  List<Object> get props => [viewer];
  Map<String, dynamic> toJson() => _$RootQueryTypeToJson(this);
}

enum PortfoliosStatus {
  ACTIVE,
  ARCHIVED,
  INACTIVE,
  ARTEMIS_UNKNOWN,
}

class ListPortfoliosQuery
    extends GraphQLQuery<RootQueryType, JsonSerializable> {
  ListPortfoliosQuery();

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.query,
        name: NameNode(value: 'listPortfolios'),
        variableDefinitions: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'viewer'),
              alias: null,
              arguments: [],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'portfolios'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: SelectionSetNode(selections: [
                      FieldNode(
                          name: NameNode(value: 'id'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'name'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'description'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null),
                      FieldNode(
                          name: NameNode(value: 'status'),
                          alias: null,
                          arguments: [],
                          directives: [],
                          selectionSet: null)
                    ]))
              ]))
        ]))
  ]);

  @override
  final String operationName = 'listPortfolios';

  @override
  List<Object> get props => [document, operationName];
  @override
  RootQueryType parse(Map<String, dynamic> json) =>
      RootQueryType.fromJson(json);
}
