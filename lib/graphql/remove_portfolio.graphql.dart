// GENERATED CODE - DO NOT MODIFY BY HAND

import 'package:meta/meta.dart';
import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import 'package:investfolio/coercers.dart';
import 'package:decimal/decimal.dart';
part 'remove_portfolio.graphql.g.dart';

@JsonSerializable(explicitToJson: true)
class RemovePortfolioResult with EquatableMixin {
  RemovePortfolioResult();

  factory RemovePortfolioResult.fromJson(Map<String, dynamic> json) =>
      _$RemovePortfolioResultFromJson(json);

  bool success;

  @override
  List<Object> get props => [success];
  Map<String, dynamic> toJson() => _$RemovePortfolioResultToJson(this);
}

@JsonSerializable(explicitToJson: true)
class RootMutationType with EquatableMixin {
  RootMutationType();

  factory RootMutationType.fromJson(Map<String, dynamic> json) =>
      _$RootMutationTypeFromJson(json);

  RemovePortfolioResult removePortfolio;

  @override
  List<Object> get props => [removePortfolio];
  Map<String, dynamic> toJson() => _$RootMutationTypeToJson(this);
}

@JsonSerializable(explicitToJson: true)
class RemovePortfolioArguments extends JsonSerializable with EquatableMixin {
  RemovePortfolioArguments({@required this.portfolioId});

  factory RemovePortfolioArguments.fromJson(Map<String, dynamic> json) =>
      _$RemovePortfolioArgumentsFromJson(json);

  final String portfolioId;

  @override
  List<Object> get props => [portfolioId];
  Map<String, dynamic> toJson() => _$RemovePortfolioArgumentsToJson(this);
}

class RemovePortfolioMutation
    extends GraphQLQuery<RootMutationType, RemovePortfolioArguments> {
  RemovePortfolioMutation({this.variables});

  @override
  final DocumentNode document = DocumentNode(definitions: [
    OperationDefinitionNode(
        type: OperationType.mutation,
        name: NameNode(value: 'removePortfolio'),
        variableDefinitions: [
          VariableDefinitionNode(
              variable: VariableNode(name: NameNode(value: 'portfolioId')),
              type: NamedTypeNode(
                  name: NameNode(value: 'String'), isNonNull: true),
              defaultValue: DefaultValueNode(value: null),
              directives: [])
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
              name: NameNode(value: 'removePortfolio'),
              alias: null,
              arguments: [
                ArgumentNode(
                    name: NameNode(value: 'input'),
                    value: ObjectValueNode(fields: [
                      ObjectFieldNode(
                          name: NameNode(value: 'portfolioId'),
                          value: VariableNode(
                              name: NameNode(value: 'portfolioId')))
                    ]))
              ],
              directives: [],
              selectionSet: SelectionSetNode(selections: [
                FieldNode(
                    name: NameNode(value: 'success'),
                    alias: null,
                    arguments: [],
                    directives: [],
                    selectionSet: null)
              ]))
        ]))
  ]);

  @override
  final String operationName = 'removePortfolio';

  @override
  final RemovePortfolioArguments variables;

  @override
  List<Object> get props => [document, operationName, variables];
  @override
  RootMutationType parse(Map<String, dynamic> json) =>
      RootMutationType.fromJson(json);
}
