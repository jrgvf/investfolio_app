// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'refresh_tokens.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Session _$SessionFromJson(Map<String, dynamic> json) {
  return Session()
    ..accessToken = json['accessToken'] as String
    ..refreshToken = json['refreshToken'] as String;
}

Map<String, dynamic> _$SessionToJson(Session instance) => <String, dynamic>{
      'accessToken': instance.accessToken,
      'refreshToken': instance.refreshToken,
    };

RootMutationType _$RootMutationTypeFromJson(Map<String, dynamic> json) {
  return RootMutationType()
    ..refresh = json['refresh'] == null
        ? null
        : Session.fromJson(json['refresh'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RootMutationTypeToJson(RootMutationType instance) =>
    <String, dynamic>{
      'refresh': instance.refresh?.toJson(),
    };

RefreshTokensArguments _$RefreshTokensArgumentsFromJson(
    Map<String, dynamic> json) {
  return RefreshTokensArguments(
    accessToken: json['accessToken'] as String,
    refreshToken: json['refreshToken'] as String,
  );
}

Map<String, dynamic> _$RefreshTokensArgumentsToJson(
        RefreshTokensArguments instance) =>
    <String, dynamic>{
      'accessToken': instance.accessToken,
      'refreshToken': instance.refreshToken,
    };
