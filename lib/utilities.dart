import 'package:artemis/artemis.dart';
import 'package:http/http.dart' as http;
import 'package:memoize/memoize.dart';

typedef VoidCallback = void Function();
typedef VoidCallbackWithParam<T> = void Function(T param);

typedef TCallback<T> = T Function();
typedef TCallbackWithParam<T, U> = T Function(U param);
typedef TCallbackWith2Params<T, U1, U2> = T Function(U1 param1, U2 param2);
typedef TCallbackWith3Params<T, U1, U2, U3> = T Function(
    U1 param1, U2 param2, U3 param3);

ArtemisClient buildArtemisClient(http.Client httpClient, String endpointUrl) =>
    ArtemisClient(endpointUrl, httpClient: httpClient);

TCallbackWith2Params<ArtemisClient, http.Client, String>
    memoizedArtemisClient() => memo2(buildArtemisClient);
