import 'package:artemis/artemis.dart';
import 'package:flutter/material.dart';
import 'package:investfolio/bloc/auth_bloc.dart';
import 'package:investfolio/graphql/list_portfolios.dart';
import 'package:investfolio/graphql/remove_portfolio.dart';
import 'package:investfolio/models/request_response.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

abstract class IGraphQLClient extends ChangeNotifier {
  static IGraphQLClient of(BuildContext context) =>
      Provider.of<IGraphQLClient>(context, listen: false);

  Future<AuthBloc> get authBloc;
  Future<http.Client> get httpClient;
  Future<ArtemisClient> get investfolioClient;

  Future<bool> login(String email, String password);
  Future<RequestResponse> signUp(String name, String email, String password);

  Future<List<Portfolio>> listPortfolios(BuildContext context);
  Future<RemovePortfolioResult> removePortfolio(
      BuildContext context, String portfolioId);
  Future<RequestResponse> createPortfolio(
      BuildContext context, Portfolio portfolio);
  Future<RequestResponse> updatePortfolio(
      BuildContext context, Portfolio portfolio);
}
