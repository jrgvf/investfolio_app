import 'package:flutter/widgets.dart';

class RequestResponse {
  RequestResponse({
    @required bool success,
    @required List errors,
  })  : success = success ?? false,
        errors = errors ?? List();

  final bool success;
  final List errors;

  String buildErrorsMessage() {
    return errors.map((error) => '${error.key}: ${error.message}').join('\n');
  }
}
