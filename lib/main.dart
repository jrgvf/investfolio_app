import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:nuvigator/nuvigator.dart';

import 'navigation/investfolio_router.dart';

void main() {
  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('google_fonts/OFL.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });

  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(Investfolio());
  });
}

class Investfolio extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Color(0xFF5E63B6),
        accentColor: Color(0xFF8488CA),
        backgroundColor: Color(0xFFF5F6FE),
        cardColor: Colors.white,
        fontFamily: 'Poppins',
        textTheme: GoogleFonts.poppinsTextTheme(
          Theme.of(context).textTheme,
        ),
        inputDecorationTheme: InputDecorationTheme(
          labelStyle: TextStyle(
            color: Color(0xFF8488CA),
          ),
        ),
      ),
      builder: Nuvigator(
        router: InvestfolioRouter(),
        screenType: materialScreenType,
        initialRoute: InvestfolioRoutes.initialScreen,
      ),
    );
  }
}
