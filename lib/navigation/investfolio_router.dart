import 'package:flutter/widgets.dart';
import 'package:investfolio/bloc/auth_bloc.dart';
import 'package:investfolio/bloc/investfolio_client.dart';
import 'package:investfolio/blocs/graphql_client.dart';
import 'package:investfolio/graphql/list_portfolios.dart';
import 'package:investfolio/screens/create_portfolio_screen.dart';
import 'package:investfolio/screens/my_home_page.dart';
import 'package:investfolio/screens/portfolios_screen.dart';
import 'package:investfolio/screens/update_portfolio_screen.dart';
import 'package:nuvigator/nuvigator.dart';
import 'package:provider/provider.dart';

import '../screens/login_screen.dart';
import '../screens/initial_screen.dart';
import '../screens/sign_up_screen.dart';

part 'investfolio_router.g.dart';

@NuRouter()
class InvestfolioRouter extends Router {
  @override
  WrapperFn get screensWrapper => (BuildContext context, Widget child) {
        AuthBloc authBloc = AuthBloc();

        return MultiProvider(
          providers: [
            ChangeNotifierProvider.value(
              value: authBloc,
              child: child,
            ),
            ChangeNotifierProvider<IGraphQLClient>(
              create: (_) => InvestfolioClient(authBloc),
              child: child,
            ),
          ],
          child: child,
        );
      };

  @NuRoute()
  ScreenRoute initialScreen() => ScreenRoute(
        builder: (_) => InitialScreen(),
      );

  @NuRoute()
  ScreenRoute loginScreen() => ScreenRoute(
        builder: (context) => LoginScreen(
          login: (String email, String password) =>
              IGraphQLClient.of(context).login(email, password),
        ),
      );

  @NuRoute()
  ScreenRoute signUpScreen() => ScreenRoute(
        builder: (context) => SignUpScreen(
          signUp: (String name, String email, String password) =>
              IGraphQLClient.of(context).signUp(name, email, password),
        ),
      );

  @NuRoute()
  ScreenRoute homePage({String title}) => ScreenRoute(
        builder: (_) => MyHomePage(title: title),
      );

  @NuRoute()
  ScreenRoute portfoliosScreen() => ScreenRoute(
        builder: (context) => PortfoliosScreen(
          listPortfolios: (context) =>
              IGraphQLClient.of(context).listPortfolios(context),
          removePortfolio: (context, portfolioId) =>
              IGraphQLClient.of(context).removePortfolio(context, portfolioId),
        ),
      );

  @NuRoute()
  ScreenRoute createPortfolioScreen() => ScreenRoute(
        builder: (_) => CreatePortfolioScreen(
          createPortfolio: (BuildContext context, Portfolio portfolio) =>
              IGraphQLClient.of(context).createPortfolio(context, portfolio),
        ),
      );

  @NuRoute()
  ScreenRoute updatePortfolioScreen({Portfolio portfolio}) => ScreenRoute(
        builder: (_) => UpdatePortfolioScreen(
          portfolio: portfolio,
          updatePortfolio: (BuildContext context, Portfolio portfolio) =>
              IGraphQLClient.of(context).updatePortfolio(context, portfolio),
        ),
      );

  @override
  Map<RouteDef, ScreenRouteBuilder> get screensMap => _$screensMap;
}
