// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'investfolio_router.dart';

// **************************************************************************
// NuvigatorGenerator
// **************************************************************************

class InvestfolioRoutes {
  static const initialScreen = 'investfolio/initialScreen';

  static const loginScreen = 'investfolio/loginScreen';

  static const signUpScreen = 'investfolio/signUpScreen';

  static const homePage = 'investfolio/homePage';

  static const portfoliosScreen = 'investfolio/portfoliosScreen';

  static const createPortfolioScreen = 'investfolio/createPortfolioScreen';

  static const updatePortfolioScreen = 'investfolio/updatePortfolioScreen';
}

class HomePageArgs {
  HomePageArgs({@required this.title});

  final String title;

  static HomePageArgs parse(Map<String, Object> args) {
    return HomePageArgs(
      title: args['title'],
    );
  }

  Map<String, Object> get toMap => {
        'title': title,
      };
  static HomePageArgs of(BuildContext context) {
    final routeSettings = ModalRoute.of(context)?.settings;
    final nuvigator = Nuvigator.of(context);
    if (routeSettings?.name == InvestfolioRoutes.homePage) {
      final args = routeSettings?.arguments;
      if (args == null)
        throw FlutterError('HomePageArgs requires Route arguments');
      if (args is HomePageArgs) return args;
      if (args is Map<String, Object>) return parse(args);
    } else if (nuvigator != null) {
      return of(nuvigator.context);
    }
    return null;
  }
}

class UpdatePortfolioScreenArgs {
  UpdatePortfolioScreenArgs({@required this.portfolio});

  final Portfolio portfolio;

  static UpdatePortfolioScreenArgs parse(Map<String, Object> args) {
    return UpdatePortfolioScreenArgs(
      portfolio: args['portfolio'],
    );
  }

  Map<String, Object> get toMap => {
        'portfolio': portfolio,
      };
  static UpdatePortfolioScreenArgs of(BuildContext context) {
    final routeSettings = ModalRoute.of(context)?.settings;
    final nuvigator = Nuvigator.of(context);
    if (routeSettings?.name == InvestfolioRoutes.updatePortfolioScreen) {
      final args = routeSettings?.arguments;
      if (args == null)
        throw FlutterError(
            'UpdatePortfolioScreenArgs requires Route arguments');
      if (args is UpdatePortfolioScreenArgs) return args;
      if (args is Map<String, Object>) return parse(args);
    } else if (nuvigator != null) {
      return of(nuvigator.context);
    }
    return null;
  }
}

extension InvestfolioRouterNavigation on InvestfolioRouter {
  Future<Object> toInitialScreen() {
    return nuvigator.pushNamed<Object>(
      InvestfolioRoutes.initialScreen,
    );
  }

  Future<Object> pushReplacementToInitialScreen<TO extends Object>(
      {TO result}) {
    return nuvigator.pushReplacementNamed<Object, TO>(
      InvestfolioRoutes.initialScreen,
      result: result,
    );
  }

  Future<Object> pushAndRemoveUntilToInitialScreen<TO extends Object>(
      {@required RoutePredicate predicate}) {
    return nuvigator.pushNamedAndRemoveUntil<Object>(
      InvestfolioRoutes.initialScreen,
      predicate,
    );
  }

  Future<Object> popAndPushToInitialScreen<TO extends Object>({TO result}) {
    return nuvigator.popAndPushNamed<Object, TO>(
      InvestfolioRoutes.initialScreen,
      result: result,
    );
  }

  Future<Object> toLoginScreen() {
    return nuvigator.pushNamed<Object>(
      InvestfolioRoutes.loginScreen,
    );
  }

  Future<Object> pushReplacementToLoginScreen<TO extends Object>({TO result}) {
    return nuvigator.pushReplacementNamed<Object, TO>(
      InvestfolioRoutes.loginScreen,
      result: result,
    );
  }

  Future<Object> pushAndRemoveUntilToLoginScreen<TO extends Object>(
      {@required RoutePredicate predicate}) {
    return nuvigator.pushNamedAndRemoveUntil<Object>(
      InvestfolioRoutes.loginScreen,
      predicate,
    );
  }

  Future<Object> popAndPushToLoginScreen<TO extends Object>({TO result}) {
    return nuvigator.popAndPushNamed<Object, TO>(
      InvestfolioRoutes.loginScreen,
      result: result,
    );
  }

  Future<Object> toSignUpScreen() {
    return nuvigator.pushNamed<Object>(
      InvestfolioRoutes.signUpScreen,
    );
  }

  Future<Object> pushReplacementToSignUpScreen<TO extends Object>({TO result}) {
    return nuvigator.pushReplacementNamed<Object, TO>(
      InvestfolioRoutes.signUpScreen,
      result: result,
    );
  }

  Future<Object> pushAndRemoveUntilToSignUpScreen<TO extends Object>(
      {@required RoutePredicate predicate}) {
    return nuvigator.pushNamedAndRemoveUntil<Object>(
      InvestfolioRoutes.signUpScreen,
      predicate,
    );
  }

  Future<Object> popAndPushToSignUpScreen<TO extends Object>({TO result}) {
    return nuvigator.popAndPushNamed<Object, TO>(
      InvestfolioRoutes.signUpScreen,
      result: result,
    );
  }

  Future<Object> toHomePage({String title}) {
    return nuvigator.pushNamed<Object>(
      InvestfolioRoutes.homePage,
      arguments: {
        'title': title,
      },
    );
  }

  Future<Object> pushReplacementToHomePage<TO extends Object>(
      {String title, TO result}) {
    return nuvigator.pushReplacementNamed<Object, TO>(
      InvestfolioRoutes.homePage,
      arguments: {
        'title': title,
      },
      result: result,
    );
  }

  Future<Object> pushAndRemoveUntilToHomePage<TO extends Object>(
      {String title, @required RoutePredicate predicate}) {
    return nuvigator.pushNamedAndRemoveUntil<Object>(
      InvestfolioRoutes.homePage,
      predicate,
      arguments: {
        'title': title,
      },
    );
  }

  Future<Object> popAndPushToHomePage<TO extends Object>(
      {String title, TO result}) {
    return nuvigator.popAndPushNamed<Object, TO>(
      InvestfolioRoutes.homePage,
      arguments: {
        'title': title,
      },
      result: result,
    );
  }

  Future<Object> toPortfoliosScreen() {
    return nuvigator.pushNamed<Object>(
      InvestfolioRoutes.portfoliosScreen,
    );
  }

  Future<Object> pushReplacementToPortfoliosScreen<TO extends Object>(
      {TO result}) {
    return nuvigator.pushReplacementNamed<Object, TO>(
      InvestfolioRoutes.portfoliosScreen,
      result: result,
    );
  }

  Future<Object> pushAndRemoveUntilToPortfoliosScreen<TO extends Object>(
      {@required RoutePredicate predicate}) {
    return nuvigator.pushNamedAndRemoveUntil<Object>(
      InvestfolioRoutes.portfoliosScreen,
      predicate,
    );
  }

  Future<Object> popAndPushToPortfoliosScreen<TO extends Object>({TO result}) {
    return nuvigator.popAndPushNamed<Object, TO>(
      InvestfolioRoutes.portfoliosScreen,
      result: result,
    );
  }

  Future<Object> toCreatePortfolioScreen() {
    return nuvigator.pushNamed<Object>(
      InvestfolioRoutes.createPortfolioScreen,
    );
  }

  Future<Object> pushReplacementToCreatePortfolioScreen<TO extends Object>(
      {TO result}) {
    return nuvigator.pushReplacementNamed<Object, TO>(
      InvestfolioRoutes.createPortfolioScreen,
      result: result,
    );
  }

  Future<Object> pushAndRemoveUntilToCreatePortfolioScreen<TO extends Object>(
      {@required RoutePredicate predicate}) {
    return nuvigator.pushNamedAndRemoveUntil<Object>(
      InvestfolioRoutes.createPortfolioScreen,
      predicate,
    );
  }

  Future<Object> popAndPushToCreatePortfolioScreen<TO extends Object>(
      {TO result}) {
    return nuvigator.popAndPushNamed<Object, TO>(
      InvestfolioRoutes.createPortfolioScreen,
      result: result,
    );
  }

  Future<Object> toUpdatePortfolioScreen({Portfolio portfolio}) {
    return nuvigator.pushNamed<Object>(
      InvestfolioRoutes.updatePortfolioScreen,
      arguments: {
        'portfolio': portfolio,
      },
    );
  }

  Future<Object> pushReplacementToUpdatePortfolioScreen<TO extends Object>(
      {Portfolio portfolio, TO result}) {
    return nuvigator.pushReplacementNamed<Object, TO>(
      InvestfolioRoutes.updatePortfolioScreen,
      arguments: {
        'portfolio': portfolio,
      },
      result: result,
    );
  }

  Future<Object> pushAndRemoveUntilToUpdatePortfolioScreen<TO extends Object>(
      {Portfolio portfolio, @required RoutePredicate predicate}) {
    return nuvigator.pushNamedAndRemoveUntil<Object>(
      InvestfolioRoutes.updatePortfolioScreen,
      predicate,
      arguments: {
        'portfolio': portfolio,
      },
    );
  }

  Future<Object> popAndPushToUpdatePortfolioScreen<TO extends Object>(
      {Portfolio portfolio, TO result}) {
    return nuvigator.popAndPushNamed<Object, TO>(
      InvestfolioRoutes.updatePortfolioScreen,
      arguments: {
        'portfolio': portfolio,
      },
      result: result,
    );
  }
}

extension InvestfolioRouterScreensAndRouters on InvestfolioRouter {
  Map<RouteDef, ScreenRouteBuilder> get _$screensMap {
    return {
      RouteDef(InvestfolioRoutes.initialScreen): (RouteSettings settings) {
        return initialScreen();
      },
      RouteDef(InvestfolioRoutes.loginScreen): (RouteSettings settings) {
        return loginScreen();
      },
      RouteDef(InvestfolioRoutes.signUpScreen): (RouteSettings settings) {
        return signUpScreen();
      },
      RouteDef(InvestfolioRoutes.homePage): (RouteSettings settings) {
        final Map<String, Object> args = settings.arguments ?? const {};
        return homePage(title: args['title']);
      },
      RouteDef(InvestfolioRoutes.portfoliosScreen): (RouteSettings settings) {
        return portfoliosScreen();
      },
      RouteDef(InvestfolioRoutes.createPortfolioScreen):
          (RouteSettings settings) {
        return createPortfolioScreen();
      },
      RouteDef(InvestfolioRoutes.updatePortfolioScreen):
          (RouteSettings settings) {
        final Map<String, Object> args = settings.arguments ?? const {};
        return updatePortfolioScreen(portfolio: args['portfolio']);
      },
    };
  }
}
