import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:investfolio/components/portfolio_item.dart';
import 'package:investfolio/graphql/list_portfolios.dart';
import 'package:investfolio/graphql/remove_portfolio.dart' as r;
import 'package:investfolio/navigation/investfolio_router.dart';
import 'package:investfolio/utilities.dart';
import 'package:nuvigator/nuvigator.dart';

class PortfolioList extends StatefulWidget {
  PortfolioList({
    Key key,
    this.portfolios,
    this.listPortfolios,
    this.removePortfolio,
  }) : super(key: key);

  final List<Portfolio> portfolios;

  final TCallbackWithParam<Future<List<Portfolio>>, BuildContext>
      listPortfolios;
  final TCallbackWith2Params<Future<r.RemovePortfolioResult>, BuildContext,
      String> removePortfolio;

  @override
  _PortfolioListState createState() => _PortfolioListState(portfolios);
}

class _PortfolioListState extends State<PortfolioList> {
  _PortfolioListState(this.portfolios);

  List<Portfolio> portfolios;

  Future<void> refreshPortfoliosList(BuildContext context) async {
    final List<Portfolio> newPortfolios = await widget.listPortfolios(context);

    setState(() => portfolios = newPortfolios);
  }

  Future<void> removePortfolio(
      BuildContext context, Portfolio portfolio, int index) async {
    final response = await widget.removePortfolio(context, portfolio.id);
    if (response != null && response.success) {
      setState(() => portfolios.removeAt(index));
    } else {
      Flushbar(
        icon: Icon(
          Icons.info_outline,
          size: 28.0,
          color: Color(0xFF5E63B6),
        ),
        title: "Não foi possível remover ${portfolio.name}",
        message: 'Tente novamente mais tarde.',
        duration: Duration(seconds: 5),
        leftBarIndicatorColor: Color(0xFF5E63B6),
        dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      )..show(context);
    }
  }

  Future<bool> showDeleteAlertDialog(Portfolio portfolio, int index,
      BuildContext context, ThemeData theme, Function removePortfolio) {
    Widget cancelButton = FlatButton(
      child: Text(
        "Cancelar",
        style: TextStyle(color: theme.primaryColor),
      ),
      onPressed: () => Navigator.of(context).pop(false),
    );

    Widget continueButton = FlatButton(
      child: Text(
        "Continuar",
        style: TextStyle(color: theme.primaryColor),
      ),
      onPressed: () {
        removePortfolio(context, portfolio, index);
        Navigator.of(context).pop(true);
      },
    );

    AlertDialog alert = AlertDialog(
      content: Text("Tem certeza que deseja remover '${portfolio.name}'?"),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      actions: [cancelButton, continueButton],
    );

    return showDialog(
        context: context, builder: (BuildContext context) => alert);
  }

  Widget emptyPortfolios(ThemeData theme) {
    return Stack(
      children: <Widget>[
        Center(
          child: Text(
            'Você ainda não possui carteiras cadastradas',
            style: TextStyle(
              color: theme.primaryColor,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        ListView()
      ],
    );
  }

  Widget portolioListView(ThemeData theme) {
    final SlidableController slidableController = SlidableController();

    return ListView.builder(
      shrinkWrap: true,
      itemCount: portfolios.length,
      itemBuilder: (context, index) {
        final portfolio = portfolios[index];

        return Slidable(
          key: Key(portfolio.id),
          closeOnScroll: false,
          controller: slidableController,
          actionPane: SlidableStrechActionPane(),
          actionExtentRatio: 0.16,
          child: PortfolioItem(portfolio: portfolio),
          dismissal: SlidableDismissal(
            closeOnCanceled: false,
            child: SlidableDrawerDismissal(),
            onWillDismiss: (actionType) {
              if (actionType == SlideActionType.secondary) {
                return showDeleteAlertDialog(
                  portfolio,
                  index,
                  context,
                  theme,
                  removePortfolio,
                );
              } else {
                Router.of<InvestfolioRouter>(context)
                    .toUpdatePortfolioScreen(portfolio: portfolio);
                return false;
              }
            },
          ),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 4.0, bottom: 4.0),
              child: IconSlideAction(
                closeOnTap: false,
                color: Colors.green,
                iconWidget: FaIcon(
                  FontAwesomeIcons.pen,
                  color: Colors.white,
                  size: 16.0,
                ),
                onTap: () => Router.of<InvestfolioRouter>(context)
                    .toUpdatePortfolioScreen(portfolio: portfolio),
              ),
            ),
          ],
          secondaryActions: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 4.0, bottom: 4.0),
              child: IconSlideAction(
                closeOnTap: false,
                color: Colors.red,
                iconWidget: FaIcon(
                  FontAwesomeIcons.trash,
                  color: Colors.white,
                  size: 16.0,
                ),
                onTap: () => showDeleteAlertDialog(
                  portfolio,
                  index,
                  context,
                  theme,
                  removePortfolio,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    ScreenUtil.init(context, width: 750, height: 1334, allowFontScaling: true);

    return RefreshIndicator(
      onRefresh: () => refreshPortfoliosList(context),
      child: (portfolios == null || portfolios.isEmpty)
          ? emptyPortfolios(theme)
          : portolioListView(theme),
    );
  }
}
