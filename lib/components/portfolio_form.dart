import 'package:card_settings/card_settings.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:investfolio/graphql/list_portfolios.dart';

class PortfolioForm extends StatefulWidget {
  PortfolioForm(
      {GlobalKey<FormState> formKey,
      Portfolio portfolio,
      String label,
      bool autoValidate})
      : _formKey = formKey,
        _label = label,
        _portfolio = portfolio,
        _autoValidate = autoValidate;

  final GlobalKey<FormState> _formKey;
  final String _label;
  final Portfolio _portfolio;
  final bool _autoValidate;

  @override
  _PortfolioFormState createState() => _PortfolioFormState();
}

class _PortfolioFormState extends State<PortfolioForm> {
  bool _showMaterialonIOS = true;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget._formKey,
      child: CardSettings.sectioned(
        showMaterialonIOS: _showMaterialonIOS,
        contentAlign: TextAlign.start,
        labelAlign: TextAlign.start,
        children: <CardSettingsSection>[
          CardSettingsSection(
            showMaterialonIOS: _showMaterialonIOS,
            header: CardSettingsHeader(
              showMaterialonIOS: _showMaterialonIOS,
              label: widget._label,
              labelAlign: TextAlign.center,
            ),
            children: <Widget>[
              _buildFieldName(),
              _buildFieldDescription(3),
              _buildFieldStatus(),
            ],
          ),
        ],
      ),
    );
  }

  CardSettingsText _buildFieldName() {
    return CardSettingsText(
      showMaterialonIOS: _showMaterialonIOS,
      label: 'Nome',
      hintText: 'Minha carteira...',
      initialValue: widget._portfolio.name,
      autocorrect: true,
      contentOnNewLine: true,
      maxLength: 50,
      showCounter: true,
      autovalidate: widget._autoValidate,
      validator: (value) {
        if (value == null || value.isEmpty) return 'O nome é obrigatório.';
        return null;
      },
      onChanged: (value) => setState(() => widget._portfolio.name = value),
    );
  }

  CardSettingsParagraph _buildFieldDescription(int lines) {
    return CardSettingsParagraph(
      showMaterialonIOS: _showMaterialonIOS,
      label: 'Descrição',
      initialValue: widget._portfolio.description,
      numberOfLines: lines,
      onChanged: (value) =>
          setState(() => widget._portfolio.description = value),
    );
  }

  CardSettingsListPicker _buildFieldStatus() {
    return CardSettingsListPicker(
      showMaterialonIOS: _showMaterialonIOS,
      label: 'Status',
      contentAlign: TextAlign.right,
      initialValue: EnumToString.parse(widget._portfolio.status),
      autovalidate: widget._autoValidate,
      options: <String>['Ativa', 'Arquivada', 'Inativa'],
      values: <String>['ACTIVE', 'ARCHIVED', 'INACTIVE'],
      validator: (String value) {
        if (value == null || value.isEmpty) return 'Selecione um status.';
        return null;
      },
      onChanged: (String value) => setState(
        () => widget._portfolio.status =
            EnumToString.fromString(PortfoliosStatus.values, value),
      ),
    );
  }
}
