import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:investfolio/models/request_response.dart';
import 'package:investfolio/navigation/investfolio_router.dart';
import 'package:nuvigator/nuvigator.dart';
import 'package:flushbar/flushbar.dart';

import '../utilities.dart';
import 'check_box_form_fied.dart';

class SignUpForm extends StatefulWidget {
  const SignUpForm({Key key, @required this.signUp}) : super(key: key);

  final TCallbackWith3Params<Future<RequestResponse>, String, String, String>
      signUp;

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final theme = GoogleFonts.poppins(
    textStyle: TextStyle(
      color: Color(0xFF5E63B6),
    ),
  );

  final _formKey = GlobalKey<FormState>();

  bool _passwordVisible = true;
  bool _submitDisabled = false;
  bool _rightInfos = false;
  bool _termsAndConditions = false;

  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  void _togglePasswordVisible() =>
      setState(() => _passwordVisible = !_passwordVisible);

  void _setSubmitDisabled(bool value) =>
      setState(() => _submitDisabled = value);

  void _toggleRightInfos(bool value) => setState(() => _rightInfos = value);

  void _toggleTermsAndConditions(bool value) =>
      setState(() => _termsAndConditions = value);

  String validateName(String value) {
    if (value.trim().isEmpty)
      return 'Informe o nome';
    else
      return null;
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);

    if (value.isEmpty)
      return 'Informe o email';
    else if (!regex.hasMatch(value))
      return 'Email inválido';
    else
      return null;
  }

  String validatePassword(String value) {
    if (value.isEmpty)
      return 'Informe a senha';
    else
      return value.length < 8 ? 'Senha muito curta' : null;
  }

  String validateRightInfos(bool value) {
    if (value)
      return null;
    else
      return 'Se estiverem corretas, confirme';
  }

  String validateTermsAndConditions(bool value) {
    if (value)
      return null;
    else
      return 'Se estiver de acordo, confirme';
  }

  Widget submitButton() => Container(
        width: ScreenUtil().setWidth(300),
        height: ScreenUtil().setHeight(80),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Color(0xFF8488CA).withOpacity(.3),
              blurRadius: 8.0,
            )
          ],
        ),
        child: RaisedButton(
          color: Color(0xFF5E63B6),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          onPressed: _submitDisabled
              ? null
              : () async {
                  if (_formKey.currentState.validate()) {
                    _setSubmitDisabled(true);

                    final response = await widget.signUp(
                      _nameController.text,
                      _emailController.text,
                      _passwordController.text,
                    );

                    _setSubmitDisabled(false);

                    if (response.success) {
                      Router.of<InvestfolioRouter>(context)
                          .pushReplacementToLoginScreen();
                      Flushbar(
                        icon: Icon(
                          Icons.check_circle_outline,
                          size: 28.0,
                          color: Color(0xFF5E63B6),
                        ),
                        title: "Cadastro realizado com sucesso",
                        message: 'Você já pode fazer o login',
                        duration: Duration(seconds: 10),
                        leftBarIndicatorColor: Color(0xFF5E63B6),
                        dismissDirection: FlushbarDismissDirection.HORIZONTAL,
                      )..show(context);
                    } else {
                      Flushbar(
                        icon: Icon(
                          Icons.info_outline,
                          size: 28.0,
                          color: Color(0xFF5E63B6),
                        ),
                        title: 'Algo deu errado...',
                        message:
                            'Verifique se as informações estão corretas\n\n${response.buildErrorsMessage()}',
                        duration: Duration(seconds: 5),
                        leftBarIndicatorColor: Color(0xFF5E63B6),
                        dismissDirection: FlushbarDismissDirection.HORIZONTAL,
                      )..show(context);
                    }
                  }
                },
          child: setUpButtonChild(),
        ),
      );

  Widget setUpButtonChild() {
    if (_submitDisabled) {
      return CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      );
    } else {
      return Text(
        "CADASTRAR",
        style: GoogleFonts.poppins(
          textStyle: theme.copyWith(
            fontSize: 18.0,
            letterSpacing: 1.0,
            fontWeight: FontWeight.w600,
            color: Colors.white,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: ScreenUtil().setHeight(500),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 15.0),
                  blurRadius: 15.0,
                ),
                BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, -10.0),
                  blurRadius: 10.0,
                ),
              ],
            ),
            child: Padding(
              padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Cadastro",
                    style: theme.copyWith(
                      fontSize: ScreenUtil().setSp(45),
                      fontWeight: FontWeight.bold,
                      letterSpacing: .6,
                    ),
                  ),
                  TextFormField(
                    controller: _nameController,
                    decoration: InputDecoration(
                      labelText: 'Nome',
                      labelStyle: theme.copyWith(
                        fontSize: ScreenUtil().setSp(26),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    keyboardType: TextInputType.text,
                    cursorColor: Color(0xFF5E63B6),
                    validator: validateName,
                  ),
                  SizedBox(height: ScreenUtil().setHeight(20)),
                  TextFormField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      labelText: 'Email',
                      labelStyle: theme.copyWith(
                        fontSize: ScreenUtil().setSp(26),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    keyboardType: TextInputType.emailAddress,
                    cursorColor: Color(0xFF5E63B6),
                    validator: validateEmail,
                  ),
                  SizedBox(height: ScreenUtil().setHeight(20)),
                  TextFormField(
                    controller: _passwordController,
                    decoration: InputDecoration(
                      labelText: 'Senha',
                      labelStyle: theme.copyWith(
                        fontSize: ScreenUtil().setSp(26),
                        fontWeight: FontWeight.w500,
                      ),
                      suffixIcon: IconButton(
                        icon: Icon(
                          _passwordVisible
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Color(0xFF5E63B6),
                        ),
                        onPressed: _togglePasswordVisible,
                      ),
                    ),
                    obscureText: _passwordVisible,
                    cursorColor: Color(0xFF5E63B6),
                    validator: validatePassword,
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: ScreenUtil().setHeight(20)),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              CheckboxFormField(
                context: context,
                initialValue: _rightInfos,
                onSaved: _toggleRightInfos,
                validator: validateRightInfos,
                title: Text(
                  'Minhas informações estão corretas',
                  style: theme.copyWith(
                    fontSize: ScreenUtil().setSp(26),
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              CheckboxFormField(
                context: context,
                initialValue: _termsAndConditions,
                onSaved: _toggleTermsAndConditions,
                validator: validateTermsAndConditions,
                title: Text(
                  'Li e aceito os Termos e Condições',
                  style: theme.copyWith(
                    fontSize: ScreenUtil().setSp(26),
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(20)),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  submitButton(),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
