import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:investfolio/graphql/list_portfolios.dart';

class PortfolioItem extends StatelessWidget {
  PortfolioItem({Key key, this.portfolio}) : super(key: key);

  final Portfolio portfolio;

  IconData _buildStatusIcon(PortfoliosStatus status) {
    switch (status) {
      case PortfoliosStatus.ACTIVE:
        return FontAwesomeIcons.wallet;
      case PortfoliosStatus.INACTIVE:
        return FontAwesomeIcons.times;
      case PortfoliosStatus.ARCHIVED:
        return FontAwesomeIcons.archive;
      default:
        return FontAwesomeIcons.question;
    }
  }

  String _buildStatusTooltip(PortfoliosStatus status) {
    switch (status) {
      case PortfoliosStatus.ACTIVE:
        return 'Carteira Ativa';
      case PortfoliosStatus.INACTIVE:
        return 'Carteira Inativa';
      case PortfoliosStatus.ARCHIVED:
        return 'Carteira Arquivada';
      default:
        return 'Status Desconhecido';
    }
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);

    return Card(
      clipBehavior: Clip.antiAlias,
      borderOnForeground: false,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.white70, width: 1),
        borderRadius: BorderRadius.circular(0.0),
      ),
      margin: EdgeInsets.only(top: 4.0, bottom: 4.0),
      child: InkWell(
        onTap: () {
          print('click');
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              leading: Tooltip(
                message: _buildStatusTooltip(portfolio.status),
                child: CircleAvatar(
                  backgroundColor: theme.primaryColor,
                  child: FaIcon(
                    _buildStatusIcon(portfolio.status),
                    color: Colors.white,
                  ),
                ),
              ),
              title: Text(
                portfolio.name,
                style: TextStyle(
                  color: theme.accentColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(portfolio.description ?? ''),
              dense: true,
            ),
          ],
        ),
      ),
    );
  }
}
