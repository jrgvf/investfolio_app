import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:investfolio/components/portfolio_form.dart';
import 'package:investfolio/graphql/list_portfolios.dart' as list;
import 'package:investfolio/models/request_response.dart';
import 'package:investfolio/navigation/investfolio_router.dart';
import 'package:nuvigator/nuvigator.dart';

import '../utilities.dart';

class UpdatePortfolioScreen extends StatefulWidget {
  UpdatePortfolioScreen({Key key, this.portfolio, this.updatePortfolio})
      : super(key: key);

  final list.Portfolio portfolio;
  final TCallbackWith2Params<Future<RequestResponse>, BuildContext,
      list.Portfolio> updatePortfolio;

  @override
  _UpdatePortfolioScreenState createState() => _UpdatePortfolioScreenState();
}

class _UpdatePortfolioScreenState extends State<UpdatePortfolioScreen> {
  bool _autoValidate = false;
  bool _submitDisabled = false;

  void _setAutoValidate(bool value) => setState(() => _autoValidate = value);

  void _setSubmitDisabled(bool value) =>
      setState(() => _submitDisabled = value);

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: theme.backgroundColor,
      appBar: AppBar(
        title: Text('InvestFolio'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            disabledColor: Colors.white,
            padding: EdgeInsets.all(16.0),
            icon: _submitDisabled
                ? CircularProgressIndicator()
                : FaIcon(FontAwesomeIcons.check, size: 16.0),
            onPressed: _submitDisabled
                ? null
                : () async => await _submitPressed(context),
          ),
        ],
      ),
      body: PortfolioForm(
        formKey: _formKey,
        label: 'Editando Carteira',
        portfolio: widget.portfolio,
        autoValidate: _autoValidate,
      ),
    );
  }

  Future<void> _submitPressed(BuildContext context) async {
    final ThemeData theme = Theme.of(context);

    if (_formKey.currentState.validate()) {
      _setSubmitDisabled(true);
      final response = await widget.updatePortfolio(context, widget.portfolio);
      _setSubmitDisabled(false);

      if (response.success) {
        Router.of<InvestfolioRouter>(context)
            .pushReplacementToPortfoliosScreen();
      } else {
        Flushbar(
          icon: Icon(Icons.info_outline, size: 28.0, color: theme.primaryColor),
          title: 'Algo deu errado...',
          message:
              'Verifique se as informações estão corretas\n\n${response.buildErrorsMessage()}',
          duration: Duration(seconds: 5),
          leftBarIndicatorColor: theme.primaryColor,
          dismissDirection: FlushbarDismissDirection.HORIZONTAL,
        )..show(context);
      }
    } else {
      _setAutoValidate(true);
    }
  }
}
