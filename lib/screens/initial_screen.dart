import 'package:flutter/material.dart';
import 'package:investfolio/bloc/auth_bloc.dart';
import 'package:investfolio/investfolio_images.dart';
import 'package:investfolio/navigation/investfolio_router.dart';
import 'package:nuvigator/nuvigator.dart';
import 'package:provider/provider.dart';

class InitialScreen extends StatefulWidget {
  @override
  _InitialScreenState createState() => _InitialScreenState();
}

class _InitialScreenState extends State<InitialScreen> {
  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 3), () => executeAfterBuild(context));

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(child: InvestfolioImages.fullLogo),
          Center(
            child: CircularProgressIndicator(),
          ),
        ],
      ),
    );
  }

  Future<void> executeAfterBuild(BuildContext context) async {
    final String accessToken =
        await Provider.of<AuthBloc>(context, listen: false).accessToken;
    if (accessToken == "")
      Router.of<InvestfolioRouter>(context).pushReplacementToLoginScreen();
    else
      Router.of<InvestfolioRouter>(context).pushReplacementToPortfoliosScreen();
  }
}
