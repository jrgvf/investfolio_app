import 'package:flutter/material.dart';
import 'package:investfolio/components/portfolio_list.dart';
import 'package:investfolio/graphql/list_portfolios.dart';
import 'package:investfolio/graphql/remove_portfolio.dart' as r;
import 'package:investfolio/navigation/investfolio_router.dart';
import 'package:investfolio/utilities.dart';
import 'package:nuvigator/nuvigator.dart';

class PortfoliosScreen extends StatefulWidget {
  PortfoliosScreen({Key key, this.listPortfolios, this.removePortfolio})
      : super(key: key);

  final TCallbackWithParam<Future<List<Portfolio>>, BuildContext>
      listPortfolios;
  final TCallbackWith2Params<Future<r.RemovePortfolioResult>, BuildContext,
      String> removePortfolio;

  @override
  _PortfoliosScreenState createState() => _PortfoliosScreenState();
}

class _PortfoliosScreenState extends State<PortfoliosScreen> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
      backgroundColor: theme.backgroundColor,
      appBar: AppBar(
        title: Text('InvestFolio'),
        centerTitle: true,
      ),
      body: FutureBuilder(
        future: widget.listPortfolios(context),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.done:
              return PortfolioList(
                portfolios: snapshot.data,
                listPortfolios: widget.listPortfolios,
                removePortfolio: widget.removePortfolio,
              );
              break;
            default:
              return Center(
                child: CircularProgressIndicator(),
              );
          }
        },
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FloatingActionButton(
          onPressed: () =>
              Router.of<InvestfolioRouter>(context).toCreatePortfolioScreen(),
          child: Icon(Icons.add),
        ),
      ),
    );
  }
}
