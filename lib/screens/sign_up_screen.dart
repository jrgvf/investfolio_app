import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:investfolio/components/sign_up_form.dart';
import 'package:investfolio/models/request_response.dart';
import 'package:investfolio/navigation/investfolio_router.dart';
import 'package:investfolio/utilities.dart';
import 'package:nuvigator/nuvigator.dart';

import '../investfolio_images.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key key, @required this.signUp}) : super(key: key);

  final TCallbackWith3Params<Future<RequestResponse>, String, String, String>
      signUp;

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 750, height: 1334, allowFontScaling: true);

    return Scaffold(
      backgroundColor: const Color(0xFFF5F6FE),
      resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 50.0),
                child: InvestfolioImages.fullLogo,
              ),
            ],
          ),
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 28.0, right: 28.0, top: 60.0),
              child: SizedBox(
                height: MediaQuery.of(context).size.height - 110,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    SizedBox(height: ScreenUtil().setHeight(200)),
                    SignUpForm(signUp: widget.signUp),
                    SizedBox(height: ScreenUtil().setHeight(70)),
                    Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Já tem conta? ",
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Router.of<InvestfolioRouter>(context)
                                .pushReplacementToLoginScreen();
                          },
                          child: Text("Faça o login",
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  color: Color(0xFF5d74e3),
                                  fontWeight: FontWeight.w600,
                                ),
                              )),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
