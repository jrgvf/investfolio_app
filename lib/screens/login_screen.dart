import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:investfolio/navigation/investfolio_router.dart';
import 'package:investfolio/utilities.dart';
import 'package:nuvigator/nuvigator.dart';

import '../components/login_form.dart';
import '../investfolio_images.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key, @required this.login}) : super(key: key);

  final TCallbackWith2Params<Future<bool>, String, String> login;

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 750, height: 1334, allowFontScaling: true);

    return Scaffold(
      backgroundColor: const Color(0xFFF5F6FE),
      resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 50.0),
                child: InvestfolioImages.fullLogo,
              ),
            ],
          ),
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 28.0, right: 28.0, top: 60.0),
              child: SizedBox(
                height: MediaQuery.of(context).size.height - 110,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    SizedBox(height: ScreenUtil().setHeight(200)),
                    LoginForm(login: widget.login),
                    SizedBox(height: ScreenUtil().setHeight(70)),
                    Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Não tem conta? ",
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Router.of<InvestfolioRouter>(context)
                                .pushReplacementToSignUpScreen();
                          },
                          child: Text("Cadastre-se",
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  color: Color(0xFF5d74e3),
                                  fontWeight: FontWeight.w600,
                                ),
                              )),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
